import { useState } from "react"

function MyComponent() {
    const [myState, changeState] = useState("Zero")
    return(
        <>
            <p>************************************</p>
            <p>--------------------------</p>
            {myState}
            <p>--------------------------</p>
            <button onClick={() => changeState(myState === "One" ? "Zero" : "One")}> Change State</button>
            <p>************************************</p>
        </>
    )
}



export default MyComponent;
