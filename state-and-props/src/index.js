import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import MyComponent from './MyComponent'

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App value="Dibya"/>
    <App value="Jyoti"/>
    <App value="Dhar"/>
    <br></br>
    <br></br>
    <br></br>
    <MyComponent/>
    <MyComponent/>
  </React.StrictMode>
);

